module FunWithStrings
    def palindrome?
        string = self.gsub(/\W/,'')
        /^#{string}$/i === string.reverse
    end

    def count_words
        string = self.strip.downcase.gsub(/\W/,' ').split(/\s+/)
        table = Hash.new(0)
        string.each { |word| table[word].nil? ? table[word] : table[word] += 1 }
        table
    end

    def anagram_groups
        words = self.strip.gsub(/\W/, ' ').split(/\s+/)
        words.group_by {|word| word.downcase.chars.sort}.values
    end
end

# make all the above functions available as instance methods on Strings:

class String
  include FunWithStrings
end
